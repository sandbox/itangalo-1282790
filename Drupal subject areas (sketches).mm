<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1316522675277" ID="ID_60304827" LINK="http://groups.drupal.org/node/172429#comment-587709" MODIFIED="1316526160403" TEXT="Drupal subject areas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      These &quot;subject areas&quot; are much broader than the &quot;achievements&quot; listed in the other mind map. See http://groups.drupal.org/node/172429#comment-587709 for some more comments and discussion.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1316527114562" ID="ID_1330659605" MODIFIED="1316527124881" POSITION="right" TEXT="Site editing">
<node CREATED="1316522689370" ID="ID_1489617810" MODIFIED="1316525944734" TEXT="Content editing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Also includes managing comments.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316522695785" ID="ID_221142175" MODIFIED="1316527276631" TEXT="Simple site configuration">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Includes managing menus, users, blocks, front page settings, etc.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1316527129833" ID="ID_1805925429" MODIFIED="1316527132799" POSITION="right" TEXT="Site building">
<node CREATED="1316522778377" ID="ID_586873064" MODIFIED="1316527276631" TEXT="Basic site building">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Includes installing Drupal, configuring fields, creating simple views, installing and using fairly simple modules (such as References, Scheduler and Automatic Nodetitles).
    </p>
  </body>
</html></richcontent>
<node CREATED="1316525826979" ID="ID_1212850045" MODIFIED="1316526494466" TEXT="Advanced site building">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Includes complex Views configuration and complex modules such as Page manager, Rules, Organic Groups and access control modules.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1316525835633" ID="ID_1454974892" MODIFIED="1316526225739" TEXT="Multilingual sites"/>
<node CREATED="1316525894539" ID="ID_135509237" MODIFIED="1316525910886" TEXT="E-commerce sites">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (This may or may not be an area of its own.)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1316526496066" ID="ID_326793823" MODIFIED="1316526504207" TEXT="Configuration export"/>
<node CREATED="1316526679137" ID="ID_643938533" MODIFIED="1316526682487" TEXT="Upgrading modules"/>
</node>
</node>
<node CREATED="1316527163494" ID="ID_1829330810" MODIFIED="1316527164539" POSITION="right" TEXT="Coding">
<node CREATED="1316526948569" ID="ID_1685333965" MODIFIED="1316527002523" TEXT="Basic coding skills">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      This involves PHP skills, knowing Drupal coding standards, using hooks and using Drupal's API to write secure code.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316527048272" ID="ID_740335255" MODIFIED="1316527077397" TEXT="Coding for important contrib modules">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      This involves writing Views plugin/handlers, as well as extending other important parts of the Drupal ecosystem.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1316527175660" ID="ID_1857292192" MODIFIED="1316527260284" POSITION="right" TEXT="System architecting">
<node CREATED="1316526812677" ID="ID_1972614827" MODIFIED="1316526816682" TEXT="Setting up Drupal servers">
<node CREATED="1316526742619" ID="ID_1403889959" MODIFIED="1316526820321" TEXT="Content migration"/>
<node CREATED="1316526822212" ID="ID_1016546505" MODIFIED="1316526897516" TEXT="Basic performace analysis and optimizing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      This basically involves site builder skills, such as setting up caching rules, being aware of performance impacts of complex architecture, etc.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316526849002" ID="ID_283064979" MODIFIED="1316527029578" TEXT="Advanced performance optimising">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      This involves dealing with load times, memory usage, slow queries, setting up reverse proxy caching, changing cache layers, etc.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1316527214353" ID="ID_1055358705" MODIFIED="1316527258131" POSITION="right" TEXT="Theming"/>
<node CREATED="1316527216929" ID="ID_1154562288" MODIFIED="1316527256074" POSITION="right" TEXT="Designing and UX"/>
<node CREATED="1316527228584" ID="ID_1385883578" MODIFIED="1316527254258" POSITION="right" TEXT="Managing projects"/>
<node CREATED="1316527239919" ID="ID_336952307" MODIFIED="1316527250891" POSITION="right" TEXT="Marketing"/>
<node CREATED="1316527168469" ID="ID_1849536495" MODIFIED="1316527263170" POSITION="right" TEXT="Community">
<node CREATED="1316526700599" ID="ID_830694660" MODIFIED="1316526790823" TEXT="Basic drupal.org skills">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Having a user account, being able to search issue queues and reporting bugs in a proper way, etc.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
