<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1316290262432" ID="ID_952744848" MODIFIED="1316290282647" TEXT="Drupal learning curves (kind of)">
<node CREATED="1316350454371" ID="ID_972380256" MODIFIED="1316516072157" POSITION="right" TEXT="System architecture branch"/>
<node CREATED="1316504217752" FOLDED="true" ID="ID_1821208416" MODIFIED="1316520141392" POSITION="right" TEXT="Coding branch">
<node CREATED="1316290600987" ID="ID_777143858" MODIFIED="1316519138456" TEXT="Use the Devel module">
<node CREATED="1316290611650" ID="ID_785048627" MODIFIED="1316293823412" TEXT="Create a patch">
<node CREATED="1316290729246" ID="ID_1141415651" MODIFIED="1316290731309" TEXT="Review a patch">
<node CREATED="1316290851298" ID="ID_1309020985" MODIFIED="1316290854657" TEXT="Review a project application">
<node CREATED="1316290859650" ID="ID_344077174" MODIFIED="1316290862199" TEXT="Post a core patch">
<node CREATED="1316290983316" ID="ID_314129891" MODIFIED="1316290989866" TEXT="Rewrite a core subsystem">
<node CREATED="1316290992973" ID="ID_1117386753" MODIFIED="1316290995251" TEXT="Be chx"/>
</node>
<node CREATED="1316292916059" ID="ID_1647633489" MODIFIED="1316292921075" TEXT="Create a security release">
<node CREATED="1316333740813" ID="ID_1287395640" MODIFIED="1316333745287" TEXT="Get involved in the security team"/>
</node>
</node>
<node CREATED="1316292483147" ID="ID_553496981" MODIFIED="1316303097146" TEXT="Become co-maintainer for a project">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      For a project where it's needed.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316293266501" ID="ID_1005652464" MODIFIED="1316293272190" TEXT="Get involved in a core initiative"/>
</node>
</node>
<node CREATED="1316292525441" ID="ID_1722339487" MODIFIED="1316292559497" TEXT="Become part of a bug squad team">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Or at least repeatedly help reduce the open issues for a project.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1316293142563" ID="ID_1768942830" MODIFIED="1316293154934" TEXT="Attend a code sprint">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      On IRC or elsewhere.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1316292516562" ID="ID_186473789" MODIFIED="1316292520706" TEXT="Close a duplicate issue"/>
</node>
<node CREATED="1316290622442" ID="ID_157030615" MODIFIED="1316290628744" TEXT="Install and use drush">
<node CREATED="1316290632282" HGAP="31" ID="ID_1326175588" MODIFIED="1316520089061" TEXT="Acquire basic git skills" VSHIFT="-4">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Such as cloning, branching, committing and merging
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1316293305900" ID="ID_41152062" MODIFIED="1316334054412" TEXT="Create a sandbox project on d.o">
<node CREATED="1316293318435" ID="ID_1470006062" MODIFIED="1316293327326" TEXT="Read the Drupal coding standards">
<node CREATED="1316302874378" ID="ID_655081346" MODIFIED="1316302882338" TEXT="Use the Coder module"/>
</node>
<node CREATED="1316293803121" ID="ID_192335000" MODIFIED="1316293807052" TEXT="Create update hooks">
<node CREATED="1316290760606" ID="ID_1757071596" MODIFIED="1316302860770" TEXT="Port a module to a new major version"/>
</node>
</node>
</node>
</node>
<node CREATED="1316333874327" ID="ID_546078993" MODIFIED="1316520073994" TEXT="Learn basic PHP">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Managing arrays, functions, and what not.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1316290749085" ID="ID_589305106" MODIFIED="1316519135392" TEXT="Write your first module">
<node CREATED="1316291865315" ID="ID_241457177" MODIFIED="1316291872954" TEXT="Write an install profile"/>
<node CREATED="1316290771500" ID="ID_1242558879" MODIFIED="1316290779378" TEXT="Use hooks">
<node CREATED="1316290820330" ID="ID_1235857990" MODIFIED="1316290826114" TEXT="Use form API">
<node CREATED="1316291027442" ID="ID_1796309400" MODIFIED="1316302942806" TEXT="Use states in form elements"/>
<node CREATED="1316290828435" ID="ID_107345500" MODIFIED="1316302960382" TEXT="Use JQuery and JavaScript">
<node CREATED="1316290837689" ID="ID_750238631" MODIFIED="1316291006410" TEXT="Develop with #AJAX"/>
</node>
<node CREATED="1316290914351" ID="ID_715229637" MODIFIED="1316290918638" TEXT="Write a Views plugin">
<node CREATED="1316290918880" ID="ID_899783453" MODIFIED="1316290922421" TEXT="Write a Views handler"/>
</node>
<node CREATED="1316302935303" ID="ID_194160946" MODIFIED="1316302940966" TEXT="Write a CTools plugin"/>
</node>
<node CREATED="1316290960798" ID="ID_311053830" MODIFIED="1316290966507" TEXT="Create a SimpleTest">
<node CREATED="1316290895952" ID="ID_1883961206" MODIFIED="1316290969487" TEXT="Write a bad judgement module"/>
<node CREATED="1316291967566" ID="ID_717242671" MODIFIED="1316292017867" TEXT="Use Devel for checking performance">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Stuff like loading times, memory usange and slow queries.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316291993197" ID="ID_806695444" MODIFIED="1316292002221" TEXT="Improve performance">
<node CREATED="1316293769394" ID="ID_1129843175" MODIFIED="1316293772316" TEXT="Use Memcached"/>
<node CREATED="1316293772922" ID="ID_1405642615" MODIFIED="1316293787490" TEXT="Use reverse proxy cache">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Such as Varnish.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1316292757561" ID="ID_1769451835" MODIFIED="1316303016232" TEXT="Use some standard Drupal functions">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      hook_permissions, hook_menu, t()...
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1316290929926" ID="ID_21126575" MODIFIED="1316290934828" TEXT="Write a Rules plugin">
<node CREATED="1316291921450" ID="ID_1448249635" MODIFIED="1316291925650" TEXT="Write a Rules data plugin"/>
</node>
<node CREATED="1316497402429" ID="ID_1134520833" MODIFIED="1316497407841" TEXT="Check out the Examples module">
<node CREATED="1316497408182" ID="ID_1347656635" MODIFIED="1316497412273" TEXT="Contribute to the Examples module"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1316504190531" FOLDED="true" ID="ID_519658859" MODIFIED="1316516079012" POSITION="right" TEXT="Theming branch">
<node CREATED="1316290347038" ID="ID_313608757" MODIFIED="1316504198131" TEXT="Install a theme">
<node CREATED="1316290385395" ID="ID_1659891787" MODIFIED="1316290400576" TEXT="Create a subtheme">
<node CREATED="1316290412753" ID="ID_537698744" MODIFIED="1316290418383" TEXT="Overwrite a theme template">
<node CREATED="1316290425625" ID="ID_198613919" MODIFIED="1316290432633" TEXT="Add new CSS file to your theme">
<node CREATED="1316290439465" ID="ID_1166434380" MODIFIED="1316290442648" TEXT="Write your own theme"/>
</node>
<node CREATED="1316291052521" ID="ID_418179135" MODIFIED="1316291088719" TEXT="Override a theme function">
<node CREATED="1316291061849" ID="ID_1618431375" MODIFIED="1316291069824" TEXT="Write a render array">
<node CREATED="1316291076640" ID="ID_286035470" MODIFIED="1316291084347" TEXT="Declare your own theme function"/>
</node>
</node>
</node>
<node CREATED="1316293540420" ID="ID_1743338378" MODIFIED="1316347325128" TEXT="Use Firebug (or a similar tool)"/>
</node>
</node>
</node>
<node CREATED="1316504161110" FOLDED="true" ID="ID_661323228" MODIFIED="1316516123037" POSITION="right" TEXT="Site building branch">
<node CREATED="1316348764762" ID="ID_812504060" MODIFIED="1316504182544" TEXT="Use a pre-installed Drupal site">
<node CREATED="1316290285609" ID="ID_119554002" MODIFIED="1316303710370" TEXT="Install Drupal">
<node CREATED="1316291117271" ID="ID_987553005" MODIFIED="1316348884360" TEXT="Install modules">
<node CREATED="1316290294150" ID="ID_453243040" MODIFIED="1316348900790" TEXT="Field your first content type">
<node CREATED="1316290305639" ID="ID_1242540714" MODIFIED="1316360962538" TEXT="Create a view">
<node CREATED="1316290328933" ID="ID_699566856" MODIFIED="1316513461380" TEXT="Use relationships and contextual filters in Views">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      This could/should be branched into more details &#8211;&#160;exposed settings, rewriting fields, grouping, aggregation&#8230;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1316291151102" ID="ID_686543999" LINK="http://nodeone.se/node/788" MODIFIED="1316306158186" TEXT="Use Views Content Panes"/>
</node>
<node CREATED="1316290314189" ID="ID_79222893" MODIFIED="1316290317403" TEXT="Create a feature">
<node CREATED="1316291769542" ID="ID_1222576986" MODIFIED="1316360881827" TEXT="Use a non-core install profile"/>
</node>
<node CREATED="1316290320045" ID="ID_684969413" MODIFIED="1316360897213" TEXT="Create a rule">
<node CREATED="1316293684765" ID="ID_1392301864" MODIFIED="1316293688270" TEXT="Use Rules components"/>
<node CREATED="1316293690453" ID="ID_626607362" MODIFIED="1316293694366" TEXT="Use Views Bulk Operations"/>
</node>
<node CREATED="1316291172053" ID="ID_452086828" MODIFIED="1316291183452" TEXT="Use Page manager">
<node CREATED="1316293696726" ID="ID_37833362" MODIFIED="1316293701490" TEXT="Use contexts in Page manager"/>
</node>
<node CREATED="1316293365266" ID="ID_1720390006" MODIFIED="1316293395142" TEXT="Use Relation or References"/>
</node>
</node>
<node CREATED="1316291188589" ID="ID_217446341" MODIFIED="1316513634733" STYLE="fork" TEXT="Discover that you don&apos;t want a module you just installed">
<node CREATED="1316291208596" ID="ID_1001591028" MODIFIED="1316513634733" STYLE="fork" TEXT="Uninstall a module">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Includes disabling, uninstalling and removing it.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1316291229947" ID="ID_1810111833" MODIFIED="1316513571573" STYLE="fork" TEXT="Get better at judging modules without installing them"/>
</node>
<node CREATED="1316292840734" ID="ID_1279565438" MODIFIED="1316513634733" STYLE="fork" TEXT="Do a module update">
<node CREATED="1316292849541" ID="ID_696155767" MODIFIED="1316513571573" STYLE="fork" TEXT="Subscribe to the Drupal security list"/>
<node CREATED="1316344754075" ID="ID_1978743732" MODIFIED="1316513571573" STYLE="fork" TEXT="Update a site to a new major Drupal version"/>
</node>
<node CREATED="1316292986681" ID="ID_1707994510" MODIFIED="1316513634733" STYLE="fork" TEXT="Create a bug report">
<node CREATED="1316293014071" ID="ID_1023614387" MODIFIED="1316513571573" STYLE="fork" TEXT="Search issue queues for bug reports"/>
<node CREATED="1316293030294" ID="ID_1554936747" MODIFIED="1316513571573" STYLE="fork" TEXT="Report a security issue"/>
</node>
<node CREATED="1316293040158" ID="ID_964228875" MODIFIED="1316513648730" STYLE="fork" TEXT="Create an issue summary"/>
<node CREATED="1316293410936" ID="ID_1566049159" MODIFIED="1316513634733" STYLE="fork" TEXT="Get a list of favourite modules">
<node CREATED="1316338038440" ID="ID_1555399007" MODIFIED="1316513571574" STYLE="fork" TEXT="Ban the PHP module"/>
</node>
</node>
<node CREATED="1316292173831" ID="ID_681890299" MODIFIED="1316333684069" TEXT="Install a new language">
<node CREATED="1316292993817" ID="ID_117167112" MODIFIED="1316513648730" TEXT="Translate interface on your site">
<node CREATED="1316292178927" ID="ID_1725027301" MODIFIED="1316345019330" TEXT="Translate strings on l.d.o"/>
</node>
<node CREATED="1316292194103" ID="ID_344260979" MODIFIED="1316292202038" TEXT="Have multilingual content">
<node CREATED="1316292203199" ID="ID_512709986" MODIFIED="1316292219376" TEXT="Create a multilingual site that kind of works">
<node CREATED="1316292220141" ID="ID_1786827596" MODIFIED="1316513461423" TEXT="Create a multilingual site that really works">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Including multilingual views, taxonomy and menus.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1316293480918" ID="ID_1194049836" MODIFIED="1316333651935" TEXT="Launch a Drupal site">
<node CREATED="1316293495981" ID="ID_872694296" MODIFIED="1316513461434" TEXT="Do basic performance things">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Like making sure the CSS and JS aggregation is turned on.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1316293633695" ID="ID_1129564481" MODIFIED="1316293642174" TEXT="Use content access modules"/>
<node CREATED="1316293650880" ID="ID_135027673" MODIFIED="1316513461446" TEXT="Build an advanced search">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Such as with Search API or Apache Solr.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1316516101733" FOLDED="true" ID="ID_766283904" MODIFIED="1316516123788" POSITION="right" TEXT="Content editing branch">
<node CREATED="1316291253490" ID="ID_901887337" MODIFIED="1316516119945" TEXT="Learn content management">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Includes posting/updating/finding content, as well as managing comments. And taxonomy term managements.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316291273538" ID="ID_1016166943" MODIFIED="1316292716104" TEXT="Learn block management">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Including some block visibility settings.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1316292665164" ID="ID_1708210833" MODIFIED="1316292734070" TEXT="Learn basic core modules and functionality">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      This includes running cron, clearing cache, managing menus, users, permissions, setting front page&#8230;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1316504204929" FOLDED="true" ID="ID_355896557" MODIFIED="1316516132556" POSITION="right" TEXT="Community branch">
<node CREATED="1316290477928" ID="ID_393517433" MODIFIED="1316345037903" TEXT="Register a d.o account">
<node CREATED="1316290525990" ID="ID_941869147" MODIFIED="1316292093665" TEXT="Answer a forum question">
<node CREATED="1316290532061" ID="ID_361350412" MODIFIED="1316292315074" TEXT="Improve a documentation page">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Either by editing it, archiving it, or moving it.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316290561540" ID="ID_374731411" MODIFIED="1316290564123" TEXT="Post an issue">
<node CREATED="1316292334137" ID="ID_1658251422" MODIFIED="1316292392471" TEXT="Show appreciation to a project maintainer">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      For example by sending an e-mail or posting an &quot;appreciation issue&quot; for the project.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1316293087092" ID="ID_207407408" MODIFIED="1316293093902" TEXT="Mark an issue as a novice task"/>
</node>
<node CREATED="1316293599322" ID="ID_1987404950" MODIFIED="1316303756969" TEXT="Add new documentation">
<node CREATED="1316333716621" ID="ID_1906711726" MODIFIED="1316333721965" TEXT="Get involved in the documentation team"/>
</node>
<node CREATED="1316290588467" ID="ID_276988945" MODIFIED="1316290594041" TEXT="Visit a Drupal meetup">
<node CREATED="1316290793099" ID="ID_966135278" MODIFIED="1316290808952" TEXT="Create a new badge">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Don't forget recursion check.)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1316290871456" ID="ID_872261406" MODIFIED="1316292083405" TEXT="Mentor someone">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Either in general, or at a Google Summer of Code project, or a GHOP project.
    </p>
  </body>
</html></richcontent>
<node CREATED="1316349338424" ID="ID_479236907" MODIFIED="1316349360680" TEXT="Mentor mentors">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Meta!
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1316292033773" ID="ID_316433702" MODIFIED="1316292039507" TEXT="Participate in Google Summer of Code"/>
<node CREATED="1316292233005" ID="ID_1252616917" MODIFIED="1316292238358" TEXT="Attend a DrupalCon or DrupalCamp">
<node CREATED="1316345458455" ID="ID_1008109107" MODIFIED="1316345462778" TEXT="Attend a DrupalCon codesprint">
<node CREATED="1316345468719" ID="ID_1314923824" MODIFIED="1316345474146" TEXT="Mentor someone at a codesprint"/>
</node>
<node CREATED="1316346309814" ID="ID_720342007" MODIFIED="1316346315243" TEXT="Attend a BoF session"/>
<node CREATED="1316347338642" ID="ID_1931780337" MODIFIED="1316347344626" TEXT="Be a volonteer at a DrupalCon"/>
<node CREATED="1316349821288" ID="ID_195295759" MODIFIED="1316349828068" TEXT="Talk to someone you don&apos;t know at a DrupalCon">
<node CREATED="1316349841817" ID="ID_1452862776" MODIFIED="1316349853506" TEXT="Have lunch (or so) with a new Drupal acquintance"/>
</node>
</node>
<node CREATED="1316293203697" ID="ID_1698028938" MODIFIED="1316293212162" TEXT="Become a member in Drupal Association">
<node CREATED="1316293231407" ID="ID_1591091427" MODIFIED="1316293237071" TEXT="Attend a DA town hall meeting"/>
</node>
<node CREATED="1316348226932" ID="ID_1483905429" MODIFIED="1316348234442" TEXT="Do a presentation at a Drupal meeting"/>
<node CREATED="1316361024586" ID="ID_1890836053" MODIFIED="1316361030565" TEXT="Help organize a Drupal meetup"/>
</node>
<node CREATED="1316348130015" ID="ID_79547377" MODIFIED="1316348134748" TEXT="Read the documentation guidelines"/>
<node CREATED="1316348236661" ID="ID_763303052" MODIFIED="1316348248647" TEXT="Explain what Drupal is to someone who doesn&apos;t know it"/>
</node>
<node CREATED="1316290544269" ID="ID_39781835" MODIFIED="1316291659332" TEXT="Get on IRC">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Especially #drupal and #drupal-support
    </p>
  </body>
</html></richcontent>
<node CREATED="1316290549236" ID="ID_1069961633" MODIFIED="1316290553250" TEXT="Answer a question on IRC"/>
<node CREATED="1316290574548" ID="ID_1572036743" MODIFIED="1316290580801" TEXT="Attend core office hours"/>
</node>
<node CREATED="1316292574449" ID="ID_639221836" MODIFIED="1316292583616" TEXT="Write a blog post about Drupal">
<node CREATED="1316292591063" ID="ID_37065289" MODIFIED="1316292595575" TEXT="Find and read on Drupal planet">
<node CREATED="1316292607142" ID="ID_1980081846" MODIFIED="1316292613279" TEXT="Get your blog feed to Drupal planet"/>
</node>
</node>
<node CREATED="1316333774427" ID="ID_1307975652" MODIFIED="1316333779755" TEXT="Report spam on drupal.org">
<node CREATED="1316333785363" ID="ID_132844931" MODIFIED="1316333806679" TEXT="Help out in the webmasters issue queue"/>
</node>
</node>
<node CREATED="1316291108647" ID="ID_108786932" MODIFIED="1316291113046" TEXT="Post a forum question">
<node CREATED="1316293179210" ID="ID_916268" MODIFIED="1316293192874" TEXT="Become a member at a g.d.o group"/>
</node>
</node>
</node>
<node CREATED="1316350113566" ID="ID_516677410" MODIFIED="1316504253009" POSITION="right" TEXT="Project management branch"/>
<node CREATED="1316463255290" ID="ID_726314277" MODIFIED="1316516140855" POSITION="right" TEXT="Desiging and UX branch"/>
<node CREATED="1316440347179" ID="ID_645322540" MODIFIED="1316516144173" POSITION="right" TEXT="Marketing branch"/>
</node>
</map>
